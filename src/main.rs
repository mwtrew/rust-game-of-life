extern crate sdl2;
extern crate rand;

use std::time::Duration;
use sdl2::pixels::Color;
use sdl2::rect::Rect;
use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use sdl2::render::WindowCanvas;

const CELL_WIDTH_PX: u32 = 5;
const GRID_SIZE: usize = 100;

trait Drawable {
    fn draw(&self, canvas: &mut WindowCanvas);
}

trait Ticks {
    fn tick(&mut self);
}

pub struct Grid {
    pub grid: Vec<Vec<bool>>,
}

impl Grid {
    fn new() -> Grid {
        let mut new_grid: Vec<Vec<bool>> = vec![];
        for i in 0..GRID_SIZE {
            new_grid.push(vec![]);
            for _j in 0..GRID_SIZE {
                new_grid[i].push(rand::random::<bool>())
            }
        }

        return Grid {
            grid: new_grid
        };
    }

    /// Gets the cell at (x, y), wrapping around on both axes if out of bounds.
    fn get_cell(&self, x: isize, y: isize) -> bool {
        let x_transformed;
        let y_transformed;
        let max = (GRID_SIZE - 1) as isize;

        if x < 0 {
            x_transformed = max;
        } else if x > max {
            x_transformed = 0
        } else {
            x_transformed = x;
        }

        if y < 0 {
            y_transformed = max;
        } else if y > max {
            y_transformed = 0
        } else {
            y_transformed = y;
        }

        return self.grid[x_transformed as usize][y_transformed as usize];
    }

    fn count_living_neighbours(&self, x: isize, y: isize) -> i8 {
        let mut count = 0;

        for delta_x in -1isize..=1 {
            for delta_y in -1isize..=1 {
                if !(delta_x == 0 && delta_y == 0) && self.get_cell(x + delta_x, y + delta_y) {
                    count += 1;
                }
            }
        }
        return count;
    }
}

impl Ticks for Grid {
    fn tick(&mut self) {
        let mut new_grid = self.grid.clone();

        for (x, row) in self.grid.iter().enumerate() {
            for (y, cell) in row.iter().enumerate() {
                let living_n = self.count_living_neighbours(x as isize, y as isize);
                if *cell {
                    if living_n < 2 {
                        // 1. A living cell with less than two live neighbors dies
                        new_grid[x][y] = false;
                    } else if living_n > 3 {
                        // 3. A living cell with more than three live neighbors dies
                        new_grid[x][y] = false;
                    } else {
                        // 2. A living cell with two or three live neighbors lives
                    }
                } else if living_n == 3 {
                    // 4. A dead cell with exactly three live neighbors becomes live
                    new_grid[x][y] = true
                }
            }
        }
        self.grid = new_grid;
    }
}

impl Drawable for Grid {
    fn draw(&self, canvas: &mut WindowCanvas) {
        for (x, row) in self.grid.iter().enumerate() {
            for (y, cell) in row.iter().enumerate() {
                if *cell {
                    canvas.set_draw_color(Color::BLACK);
                } else {
                    canvas.set_draw_color(Color::WHITE);
                }
                canvas.fill_rect(Rect::new(
                    x as i32 * CELL_WIDTH_PX as i32,
                    y as i32 * CELL_WIDTH_PX as i32,
                    CELL_WIDTH_PX, CELL_WIDTH_PX)
                ).map_err(|e| e.to_string()).unwrap();
            }
        }
    }
}

pub fn main() -> Result<(), String> {
    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();

    let window = video_subsystem.window("Game of Life",
                                        CELL_WIDTH_PX * GRID_SIZE as u32,
                                        CELL_WIDTH_PX * GRID_SIZE as u32)
        .position_centered()
        .build()
        .unwrap();

    let mut canvas = window.into_canvas().build().unwrap();
    let mut event_pump = sdl_context.event_pump().unwrap();

    let mut grid = Grid::new();

    'running: loop {
        canvas.set_draw_color(Color::BLACK);
        canvas.clear();
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit { .. } |
                Event::KeyDown { keycode: Some(Keycode::Escape), .. } => {
                    break 'running;
                }
                _ => {}
            }
        }
        grid.tick();
        grid.draw(&mut canvas);
        canvas.present();
        std::thread::sleep(Duration::new(0, 50_000_000u32));
    }
    Ok(())
}